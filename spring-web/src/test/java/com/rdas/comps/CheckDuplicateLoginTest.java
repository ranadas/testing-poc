package com.rdas.comps;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;

/**
 * Created by x148128 on 25/01/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = WebAppConfigurator.class)
public class CheckDuplicateLoginTest {


	@Autowired
	private CheckDuplicateLogin checkDuplicateLogin;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSayingHellIsCalled() {
		assertThat(checkDuplicateLogin).isNotNull();
		checkDuplicateLogin.sayHello();
		//verify(checkDuplicateLogin, times(1)).sayHello("was called exactly three times");
	}

	@Test
	public void testThatCheckDuplicates() {
		checkDuplicateLogin.checkDuplicates("rdas");
	}

}