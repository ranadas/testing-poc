package com.rdas.webfilter;

import com.rdas.comps.CheckDuplicateLogin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

@Slf4j
public class HttpRequestTypeFilter implements Filter {

	@Autowired
	private CheckDuplicateLogin checkDuplicateLogin;

	public void init(FilterConfig filterConfig) throws ServletException {
		ServletContext servletContext = filterConfig.getServletContext();
		WebApplicationContext webApplicationContext =
				WebApplicationContextUtils.getWebApplicationContext(servletContext);

		AutowireCapableBeanFactory autowireCapableBeanFactory =
				webApplicationContext.getAutowireCapableBeanFactory();

		autowireCapableBeanFactory.configureBean(this, "checkDuplicateLogin");
	}

	public void doFilter(ServletRequest request, ServletResponse response,
	                     FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpServletRequest = (HttpServletRequest) request;


		if (isValidRequestType(httpServletRequest)) {
			chain.doFilter(request, response);
		} else {

			log.info("Invalid request – " + httpServletRequest.getMethod() + " Request type not supported");

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<html><head><title>H&amp;R Block- Error</title></head><body>");
			out.println("<h2>Error</h2>");
			out.println("<div style='color:red;'><strong>Your session has ended.</strong></div>");
			out.println("<div>Please close your browser and try again.</div>");
			out.println("</body></html>");
			out.close();
		}
	}

	public void destroy() {
	}

	private boolean isValidRequestType(HttpServletRequest request) {
		String requestMethod = request.getMethod();
		log.info("\n Requested by : {}\n", requestMethod);
		checkDuplicateLogin.sayHello();
		return ("POST".equalsIgnoreCase(requestMethod) || "GET".equalsIgnoreCase(requestMethod));
	}
}