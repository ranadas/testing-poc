package com.rdas.controller;

import com.rdas.comps.CheckDuplicateLogin;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * Created by x148128 on 11/23/2015.
 */
@Slf4j
@Controller
public class HelloWorldController {

    @Autowired
    private CheckDuplicateLogin checkDuplicateLogin;

    @RequestMapping("/")
    public String helloWorld(Model model) {
        model.addAttribute("message", "Hello World!");
        return "index";
    }

    @RequestMapping("/index")
    public String index(ModelMap model) {
        model.addAttribute("message", "Message from index Controller.");
        return "index";
    }

    @RequestMapping("/welcome")
    public ModelAndView helloWorld() {
        log.info("\n #### servicing welcome\n");
        String message = "<br><div style='text-align:center;'>"
                + "<h5>***** Hello World, SpringMVC </h5>" +
                "This message is from HelloWorldController***</div>";
        ModelAndView modelAndView = new ModelAndView("welcome");
        modelAndView.addObject("newmsg", "new welcome");
        modelAndView.addObject("message", message);

        return modelAndView;
    }

    @RequestMapping("/wel")
    public ModelAndView welcome() {
        ModelAndView model = new ModelAndView("welcome");
        String message = "<br><div style='text-align:center;'>"
                + "<h5>********** Hello World, Spring MVC </h5>" +
                "This message is from HelloWorldController***</div><br><br>";
        model.addObject("newmsg", "a new message ");
        model.addObject("message", message);
        return model;
    }
}