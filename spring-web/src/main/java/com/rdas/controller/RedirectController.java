package com.rdas.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Created by x148128 on 01/29/2016.
 */
@Slf4j
@Controller
public class RedirectController {

    @RequestMapping(value="/login", method = RequestMethod.POST )
    public String login(Model model, RedirectAttributes redirectAttributes)  {
//        redirectAttributes.addFlashAttribute("message", "success logout");
        System.out.println("/login");
        return "redirect:loginScreen";
    }


    @RequestMapping("/loginScreen")
    public ModelAndView loginSCreen() {
        log.info("\n #### new loginScreen\n");
        ModelAndView modelAndView = new ModelAndView("welcome");
        modelAndView.addObject("newmsg", "This is the Login Screen ");
        return modelAndView;
    }
}