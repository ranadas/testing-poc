package com.rdas.controller;

import com.rdas.comps.UserService;
import com.rdas.model.RegisteredUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Created by x148128 on 11/23/2015.
 * http://www.mkyong.com/spring-mvc/spring-mvc-form-handling-example/
 * TODO ADD some validations
 */
@Slf4j
@Controller
public class RegistrationFormController {

    @Autowired
    private UserService userService;
    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;


    @RequestMapping(value = "/1", method = RequestMethod.GET)
    public String displayRegistrationForm(Model model) {
        model.addAttribute("message", "Hello World! from spring 1");

        model.addAttribute("userForm", userService.get("rana das"));//new RegisteredUser());
        model.addAttribute("countryList", userService.getAllCountries());
        model.addAttribute("communityList", userService.getAllCommunities());

        return "first";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveOrUpdate(@ModelAttribute("userForm") @Validated RegisteredUser user,
                               BindingResult result,
                               Model model,
                               final RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            log.info("Errors ", result);
        }
        model.addAttribute("message", String.format("Saved user %s ", user.getName()));
        model.addAttribute( "endPoints", requestMappingHandlerMapping.getHandlerMethods().keySet() );
        log.info("save or update user {}", userService.get(user.getName()));
        return "success";
    }
}