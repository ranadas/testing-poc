package com.rdas.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by rdas on 09/02/2016.
 */
@Getter
@Setter
public class Community {
    private String key;
    private String value;

    public Community(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
