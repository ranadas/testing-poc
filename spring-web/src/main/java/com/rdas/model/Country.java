package com.rdas.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by rdas on 09/02/2016.
 */
@Getter
@Setter
public class Country {
    private int countryId;
    private String countryName;

    public Country(int countryId, String countryName) {
        this.countryId = countryId;
        this.countryName = countryName;
    }
}
