package com.rdas.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Created by rdas on 09/02/2016.
 */
@Getter @Setter @ToString
public class RegisteredUser {
    private String name;
    private String password;
    private String gender;
    private String country;
    private List countryList;
    private String aboutYou;
    private String[] community;
    private List communityList;
    private Boolean mailingList;
    private int signIn;
}
