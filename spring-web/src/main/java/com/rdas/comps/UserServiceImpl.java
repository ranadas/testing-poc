package com.rdas.comps;

import com.rdas.model.Community;
import com.rdas.model.Country;
import com.rdas.model.RegisteredUser;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserServiceImpl implements UserService {

	@Override
	public void add(RegisteredUser user) {
		//Persist the user object here. 
		System.out.println("User added successfully");

	}

	@Override
	public List getAllCountries() {
		List countryList = new ArrayList();
		countryList.add(new Country(1,"India"));
		countryList.add(new Country(2,"USA"));
		countryList.add(new Country(3,"UK"));
		return countryList;
	}

	@Override
	public List getAllCommunities() {
		List communityList = new ArrayList();
		communityList.add(new Community("Spring","Spring"));
		communityList.add(new Community("Hibernate","Hibernate"));
		communityList.add(new Community("Struts","Struts"));
		return communityList;
	}

	@Override
	public RegisteredUser get(String name) {
		RegisteredUser user = new RegisteredUser();
		user.setName(name);
		user.setPassword("passwd");
		user.setCountry("USA");
		return user;
	}
}
