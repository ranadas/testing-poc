package com.rdas.comps;

import java.util.List;

import com.rdas.model.Community;
import com.rdas.model.Country;
import com.rdas.model.RegisteredUser;

public interface UserService {

	public void add(RegisteredUser user);
	public RegisteredUser get(String name);
	public List<Country> getAllCountries();
	public List<Community> getAllCommunities();
}
