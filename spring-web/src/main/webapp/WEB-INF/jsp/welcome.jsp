<!doctype html>
<%@page session="false" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Spring MVC - Hello World Spring MVCExample</title>
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery-2.2.0.js" />"></script>
    <script src="<c:url value="/resources/js/bootstrap.js" />"></script>
    <c:url var="home" value="/" scope="request"/>
</head>
<body>
<div class="form-group">
    &nbsp;
</div>
<span class="label label-info"><c:out value="${newmsg}" default="ranadas"/></span>

<div class="form-group">
    &nbsp;
</div>
<h4><span class="label label-default">${message}</span></h4>

<h5>Contact Manager</h5>

<div class="pull-left">
    <div class="input-append">
        <input type="text"
               required
               autofocus
               ng-model="contact.name"
               name="name"
               placeholder=""/>
    </div>
</div>
</body>
</html>