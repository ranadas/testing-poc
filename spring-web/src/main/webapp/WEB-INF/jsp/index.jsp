<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jstl/sql" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Spring MVC Web  - Index</title>
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
</head>
<body>
<br/>

<div style="text-align:center">
    <h3>
        <%--<a href="welcome.html"> click here to welcome.html Screen. </a>--%>
        <a href="welcome"> click here to welcome Screen. </a>
        <br/>
    </h3>

    <div class="jumbotron">
        <p>&nbsp;</p>
        <p>motto goes here.</p>
        <p>&nbsp;</p>
        <p><a class="btn btn-primary btn-lg">Learn more</a></p>
    </div>
</div>
</body>
</html>