<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jstl/sql" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form"%>

<html>
<head>
    <title>(1) - spring web </title>
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
</head>
<body>
<div class="form-group">
    &nbsp;
</div>
<div class="container">
    <div class="col-md-6 col-md-offset-3">
        <!--
        <form name="loginForm" role="form">
            <fieldset>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control" required/>
                    <span class="help-block">Email is required</span>
                    <span class="help-block">Email is invalid</span>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control" required/>
                    <span class="help-block">Password is required</span>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Login</button>
                    <a href="#/register" class="btn btn-link">Register</a>
                </div>
            </fieldset>
        </form>
        -->
        <%--http://stackoverflow.com/questions/21495616/difference-between-modelattribute-and-commandname-atributes-in-form-tag-in-sprin--%>
        <form:form method="POST" modelAttribute="userForm" action="/save">
            <%--this is same as modelAttribute commandName="userForm"--%>
            <table>
                <tr>
                    <td>User Name :</td>
                    <td><form:input path="name"/></td>
                </tr>
                <tr>
                    <td>Password :</td>
                    <td><form:password path="password"/></td>
                </tr>
                <tr>
                    <td>Gender :</td>
                    <td>
                        <form:radiobutton path="gender" value="M" label="M"/>
                        <form:radiobutton path="gender" value="F" label="F"/>
                    </td>
                </tr>
                <tr>
                    <td>Country :</td>
                    <td>
                        <form:select path="country">
                            <form:option value="0" label="Select"/>
                            <form:options items="${countryList}" itemValue="countryId" itemLabel="countryName"/>
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td>About you :</td>
                    <td><form:textarea path="aboutYou"/></td>
                </tr>
                <tr>
                    <td>Community :</td>
                    <td><form:checkboxes path="communityList" items="${communityList}" itemValue="key"
                                         itemLabel="value"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <form:checkbox path="mailingList" label="Would you like to join our mailinglist?"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Register"></td>
                </tr>
            </table>
        </form:form>
    </div>
</div>
</body>
</html>