<!doctype html>
<%@page session="false" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Spring MVC - Hello World Spring MVCExample</title>
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery-2.2.0.js" />"></script>
    <script src="<c:url value="/resources/js/bootstrap.js" />"></script>
    <c:url var="home" value="/" scope="request"/>
</head>
<body>
<div class="form-group">
    &nbsp;
</div>

<div class="form-group">
    &nbsp;
</div>
<h5>Registration details</h5>
<span class="label label-info"><c:out value="${message}" default="ranadas"/></span>

<div class="pull-left">
</div>
<table class="table table-bordered table-striped table-sm">
    <thead class="thead-inverse">
    <tr>
        <th>path </th>
        <th>methods </th>
        <th>consumes </th>
        <th>produces </th>
        <th>params</th>
        <th>headers</th>
        <th>custom</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${endPoints}" var="endPoint">
        <tr>
            <td>${endPoint.patternsCondition}</td>
            <td>${endPoint.methodsCondition}</td>
            <td>${endPoint.consumesCondition}</td>
            <td>${endPoint.producesCondition}</td>
            <td>${endPoint.paramsCondition}</td>
            <td>${endPoint.headersCondition}</td>
            <td>${empty endPoint.customCondition ? "none" : endPoint.customCondition}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>