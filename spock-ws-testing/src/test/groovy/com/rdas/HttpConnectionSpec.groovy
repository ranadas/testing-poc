package com.rdas

import com.rdas.utils.HttpUtils
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import com.squareup.okhttp.Response
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import spock.lang.Shared
import spock.lang.Specification

/**
 * Created by rdas on 31/01/2016.
 */
@Slf4j
class HttpConnectionSpec extends Specification {

    def jsonUserAgentResponse = '''{
                                    "user-agent": "okhttp/2.7.2"
                                }'''

    @Shared OkHttpClient client = new OkHttpClient()

    def "make connection and print headers"() {
        given:
        def url = "http://httpbin.org/user-agent"

        when:
        Request request = new Request.Builder()
                .url(url)
                .build();

        then:
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        HttpUtils.printResponseHeaders(response)

    }

    def "make connection and send GET request"() {
        given:
        def url = "http://httpbin.org/user-agent"

        when:
        Request request = new Request.Builder()
                .url(url)
                .build();

        then:
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        assert new JsonSlurper().parseText(response.body().string()) == new JsonSlurper().parseText(jsonUserAgentResponse)
    }
}


