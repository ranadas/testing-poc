package com.rdas.utils

import com.squareup.okhttp.Headers
import com.squareup.okhttp.Response

/**
 * Created by rdas on 31/01/2016.
 */
class HttpUtils {

    static def printResponseHeaders(Response response) {
        Headers responseHeaders = response.headers();
        responseHeaders.each {
            println it
        }
    }
}
