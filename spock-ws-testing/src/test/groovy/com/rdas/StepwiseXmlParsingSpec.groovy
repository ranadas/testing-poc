package com.rdas

import groovy.util.logging.Slf4j
import groovy.util.slurpersupport.NodeChild
import groovy.util.slurpersupport.NodeChildren
import groovy.xml.StreamingMarkupBuilder
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

import static org.fest.assertions.Assertions.assertThat

/**
 * Created by rdas on 30/01/2016.
 * Order is maintained
 */
@Stepwise
@Slf4j
class StepwiseXmlParsingSpec extends Specification {
    @Shared
            buffer = ""
    @Shared
    int testCount
    @Shared
            fileContents

    def setupSpec() {
        log.debug('do the setups here')
        fileContents = new File('spock-ws-testing/src/test/resources/sampleSaml.xml').text
        //println fileContents
    }

    @Ignore
    def 'Read lines of a text file '() {
        setup:
        testCount++
        buffer = "rana"
        log.debug('{} in TestCase{} ', buffer, testCount)

        when:
        def number = 0

        then:
        new File('spock-ws-testing/src/test/resources/sampleText.txt').eachLine { line ->
            number++;
            println "$number: $line";
        }
    }

    def 'sample spock case 2'() {
        setup:
        testCount++
        log.debug('{} In TestCase{} ', buffer, testCount)
    }

    @Ignore
    def "process xml from file"() {
        setup:
        testCount++
        when:
        def root = new XmlParser(false, false).parseText(fileContents)

        then:
        assert root instanceof Node
        NodeList allTheNodesInTheTree = root.breadthFirst()
        NodeList allTheDepthFirstNodesInTheTree = root.depthFirst()
//        allTheDepthFirstNodesInTheTree.findAll { println it.name() }
        allTheDepthFirstNodesInTheTree.each { println it.name() }
        println allTheNodesInTheTree
    }

    def "determine elements"() {
        setup: "fileContents String is not empty"
        assert fileContents != null
        when:
        def root = new XmlSlurper().parseText(fileContents)

        then:
        assertThat(root).isNotNull()

        when: "get the Children of root node"
        NodeChildren childs = root.children()
        then: "There are two nodes of root, header and body"
        assertThat(childs.size()).isEqualTo(2)
        and: "quickly print the children"
        childs.each {
            it->
            println it.name()
        }

        when: "get the header node and remove from root"
        NodeChild header = root.depthFirst().find { it.name() == 'Header' }
        header.replaceNode {}
        def newTodo = new StreamingMarkupBuilder().bind {
            mkp.yield root
        }


        then: "get the Children of the new root node and make sure its only one"
        def newRoot =  new XmlSlurper().parseText(newTodo.toString())
        NodeChildren newChilds = newRoot.children()
        assertThat(newChilds.size()).isEqualTo(1)

    }
}


