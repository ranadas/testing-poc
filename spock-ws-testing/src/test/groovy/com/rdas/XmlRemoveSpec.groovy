package com.rdas

import groovy.util.slurpersupport.NodeChildren
import groovy.xml.StreamingMarkupBuilder
import spock.lang.Specification

import static org.fest.assertions.Assertions.assertThat

/**
 * Created by rdas on 01/02/2016.
 */
class XmlRemoveSpec extends Specification {
    def fileContents = new File('spock-ws-testing/src/test/resources/sampleSaml.xml').text
    def "find an element from root and remote it"() {
        given:
        true
        when: "parse the xml string"
        def root = new XmlSlurper().parseText(fileContents)

        then:
        assertThat(root).isNotNull()

        when: "get the Children of root node"
        NodeChildren childs = root.children()
        then: "There are two nodes of root, header and body"
        assertThat(childs.size()).isEqualTo(2)

        when: "get the header node and remove from root"
        def header = root.depthFirst().find { it.name() == 'Header' }
        header.replaceNode {}
        def newTodo = new StreamingMarkupBuilder().bind {
            mkp.yield root
        }.toString()

        then: "get the Children of the new root node and make sure its only one"
        def newRoot =  new XmlSlurper().parseText(newTodo.toString())
        NodeChildren newChilds = newRoot.children()
        assertThat(newChilds.size()).isEqualTo(1)

    }
}
