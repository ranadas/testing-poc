package com.rdas.specs

import geb.spock.GebReportingSpec
import lombok.extern.slf4j.Slf4j
import spock.lang.Ignore


/**
 * Created by rdas on 24/01/2016.
 */
@Slf4j
class SpringWebIndexSpec extends GebReportingSpec {

    @Ignore
    def 'load index page from Springweb Project and click the link to go to welcome page'() {
        go "http://localhost:8080/"
        $('a', text: contains('welcome')).click()

        expect:
        driver.currentUrl == "http://localhost:8080/welcome"
    }

    def 'load duckduckgo home'() {
        given: "in duckduckgo home page"
        go "http://www.duckduckgo.com"
        $('a', text: contains('duckduckgo')).click()

        expect:
        driver.currentUrl == "https://duckduckgo.com/"
    }


    def 'Search Groovy Browser Automation in duckduckgo'() {
        given: "in duckduckgo home page"
        go "http://www.duckduckgo.com"

        when: "search for 'Groovy Browser Automation'"
        $("#search_form_homepage").q = "Groovy Browser Automation"
        $("#search_button_homepage").click()

        then: "first result is the web site"
        assert $("#links").find(".links_main a", 0).attr("href") == "http://www.gebish.org/"
    }
}
