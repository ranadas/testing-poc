package com.rdas.pages

import geb.Page

/**
 * Created by rdas on 24/01/2016.
 */
class SpringWebIndexPage extends Page {

    static url = '/'

    static at = { title.startsWith 'Spring MVC Web' }

    static content = {
        heading { $("h3")}
//        //cartStatus { module CartStatusModule, $('#cart-status-header') }
    }
}
