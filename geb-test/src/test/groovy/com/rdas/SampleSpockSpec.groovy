package com.rdas

import groovy.json.JsonBuilder
import groovy.util.logging.Slf4j
import groovyx.net.http.RESTClient
import spock.lang.Specification
import static groovyx.net.http.ContentType.TEXT


/**
 * Created by rdas on 25/01/2016.
 */
@Slf4j
class SampleSpockSpec extends Specification {

    def weatherAPiKey="44db6a862fba0b067b1930da0d769e98"

    def 'sample spock test'(){
        setup:
        log.debug('woo hoo')
    }

    def 'Check if the weather in Eindhoven can be found'() {
        given:
        RESTClient restClient = new RESTClient("http://api.openweathermap.org")
        and:
        String city = "Eindhoven,nl"
        and:
        restClient.setHeaders(Accept: 'application/json')


        when:
        def response = restClient.get( path: '/data/2.5/weather', query: ['q' : city, 'appid': weatherAPiKey])
        log.debug(response.data.toString())
        //def slurper = new groovy.json.JsonSlurper().parse(response.responseData)
//        builder = new JsonBuilder()
//        builder(response.responseData)
//        println builder.toString()
        println response.responseData.toMapString()

        then:
        response.status == 200

        and:
        response.responseData.name == "Eindhoven"
    }

    //http://httpbin.org/
    def "invoke http bin for xml response" () {
        given :
        RESTClient restClient = new RESTClient("http://httpbin.org")

        when:
        def response = restClient.get( path: '/xml')
        log.debug(response.data.toString())

        then:
        response.status == 200

    }

}


