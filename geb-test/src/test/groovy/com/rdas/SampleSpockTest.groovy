package com.rdas

import groovy.util.logging.Slf4j
import spock.lang.Specification

/**
 * Created by rdas on 25/01/2016.
 */
@Slf4j
class SampleSpockTest extends Specification {

    def "length of Spock's and his friends' names"() {
        expect:
        name.size() == length

        where:
        name     | length
        "Spock"  | 5
        "Kirk"   | 4
        "Scotty" | 6
    }
}


